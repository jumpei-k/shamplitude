//
//  InitailViewController.swift
//  SHAmplitude
//
//  Created by Jumpei Katayama on 11/24/15.
//  Copyright © 2015 StreetHawk. All rights reserved.
//

import UIKit

class InitailViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var userIdField: UITextField! {
        didSet {
            userIdField.delegate = self
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func tapStartButton(sender: AnyObject) {
        
        let progressHUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        progressHUD.labelText = "Entering..."
        
        NSUserDefaults.standardUserDefaults().setValue(userIdField.text, forKey: UserdefaultKey.AmplitudeUserId)
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: UserdefaultKey.HasLaunchedOnce)
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: UserdefaultKey.HasUserIdForAmplitude)
        NSUserDefaults.standardUserDefaults().synchronize()
        
        
        Amplitude.instance().trackingSessionEvents = true
        Amplitude.instance().initializeApiKey("6f224e9c8ab6eef9df5f20d33a169f99", userId: userIdField.text)
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0)) {
            
            dispatch_async(dispatch_get_main_queue()) {
                progressHUD.hide(true)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewControllerWithIdentifier("HomeViewControlle")
                self.presentViewController(vc, animated: true, completion: nil)
                // ...Run something once we're done with the background task...
            }
        }
        
    }
}
