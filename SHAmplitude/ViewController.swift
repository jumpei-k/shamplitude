//
//  ViewController.swift
//  SHAmplitude
//
//  Created by Jumpei Katayama on 11/23/15.
//  Copyright © 2015 StreetHawk. All rights reserved.
//

import UIKit

struct UserIdentifierKey {
    static let gender = "gender"
    static let name = "name"
    static let age = "age"
    static let cuid = "cuid"
    static let eventKey = "event key"
    static let eventValue = "event value"
}

struct UserdefaultKey {
    static let HasLaunchedOnce = "HasLaunchedOnce"
    static let HasUserIdForAmplitude = "HasUserIdForAmplitude"
    static let AmplitudeUserId = "AmplitudeUserId"
}


class ViewController: UIViewController, UITextFieldDelegate {

    var activeFiled: UITextField? {
        didSet {
            if activeFiled != nil {
                activeFiled!.delegate = self
            }

        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var nameField: UITextField! {
        didSet {
            nameField.delegate = self
        }
    }
    @IBOutlet weak var genderSegment: UISegmentedControl!
    
    var segmentIndex: Int! {
        return genderSegment.selectedSegmentIndex
    }
    
    @IBOutlet weak var ageField: UITextField! {
        didSet {
            ageField.delegate = self
        }
    }
    @IBOutlet weak var cuidField: UITextField! {
        didSet {
            cuidField.delegate = self
        }
    }
    @IBOutlet weak var eventKeyField: UITextField! {
        didSet {
            eventKeyField.delegate = self
        }
    }
    @IBOutlet weak var eventValueField: UITextField! {
        didSet {
            eventValueField.delegate = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        registerForKeyboardNotifications()
    }
    
    deinit {
        deregisterFromKeyboardNotifications()
    }
 
    func refreshForm() {
        nameField.text = ""
        ageField.text = ""
        cuidField.text = ""
        eventKeyField.text = ""
        eventValueField.text = ""
        
    }
    
    @IBAction func Identify(sender: AnyObject) {
        let name = nameField.text
        let gender = genderSegment.titleForSegmentAtIndex(segmentIndex)
        let age = ageField.text
        let identify: AMPIdentify = AMPIdentify().set(UserIdentifierKey.gender, value: gender).set(UserIdentifierKey.name, value: name).set(UserIdentifierKey.age, value: age)
        
        let progressHUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        progressHUD.labelText = "Sending..."
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0)) {
            
            Amplitude.instance().identify(identify)
            
            dispatch_async(dispatch_get_main_queue()) {
                progressHUD.hide(true)
                
                // ...Run something once we're done with the background task...
            }
        }

        

//        Amplitude.instance().logEvent("Tap identify button")
    }
    
    
    @IBAction func sendEvent(sender: AnyObject) {
        let cuid: String! = cuidField.text
        let eventValue: String! = eventValueField.text
        let eventKey: String! = eventKeyField.text
        let eventProperty = [UserIdentifierKey.cuid: cuid, UserIdentifierKey.eventKey: eventKey, UserIdentifierKey.eventValue: eventValue];
        print(eventProperty)
        
        let progressHUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        progressHUD.labelText = "Sending..."
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0)) {
            
            Amplitude.instance().logEvent("Tap send event button", withEventProperties: eventProperty)
            
            dispatch_async(dispatch_get_main_queue()) {
                progressHUD.hide(true)
                self.refreshForm()
                // ...Run something once we're done with the background task...
            }
        }
        
        
    }
    
    
    func registerForKeyboardNotifications()
    {
        //Adding notifies on keyboard appearing
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWasShown:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillBeHidden:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    
    func deregisterFromKeyboardNotifications()
    {
        //Removing notifies on keyboard appearing
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWasShown(notification: NSNotification)
    {
        print("keyboard was shown")
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.scrollEnabled = true
        let info : NSDictionary = notification.userInfo!
        var keyboardSize: CGSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)!.CGRectValue().size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize.height
        if let activeFiled = activeFiled
        {
            if (!CGRectContainsPoint(aRect, activeFiled.frame.origin))
            {
                print("waht up")
                self.scrollView.scrollRectToVisible(activeFiled.frame, animated: true)
            }
        }
        
        
    }
    
    
    func keyboardWillBeHidden(notification: NSNotification)
    {
        //Once keyboard disappears, restore original positions
        let info : NSDictionary = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.scrollEnabled = false
        
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {

    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {

    }
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        activeFiled = textField
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
//        activeFiled = nil
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool { // called when 'return' key pressed. return NO to ignore.
        activeFiled = nil
        textField.resignFirstResponder()
        return true
    }


}

